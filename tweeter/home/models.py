from django.db import models
from django.conf import settings

class Tweet(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete = models.CASCADE)
    content = models.CharField(max_length = 140)
    timestamp = models.DateTimeField(auto_now_add = True)
    update = models.DateTimeField(auto_now_add = True)
