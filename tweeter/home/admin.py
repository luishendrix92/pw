from django.contrib import admin

from .models import Tweet

@admin.register(Tweet)
class AdminTweet(admin.ModelAdmin):
    list_display = [
        "id",
        "user",
        "content",
        "timestamp"
    ]
