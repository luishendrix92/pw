from django.urls import path
from .views import ListView

urlpatterns = [
    path('tweets/', ListView.as_view(), name = "tweets"),
]

