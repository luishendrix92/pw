from rest_framework import serializers
from django.contrib.auth import get_user_model

from home.models import Tweet

class UserDisplaySerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = [
            "username",
            "first_name",
            "last_name",
        ]

class TweetModelSerializer(serializers.ModelSerializer):
    user = UserDisplaySerializer()

    class Meta:
        model = Tweet
        fields = ["user", "content"]
