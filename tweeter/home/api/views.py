from rest_framework import generics
from home.models import Tweet
from .serializers import TweetModelSerializer

class ListView(generics.ListAPIView):
    queryset = Tweet.objects.all()
    serializer_class = TweetModelSerializer
