from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

from .models import Tweet

class FormTweet(forms.ModelForm):
    content = forms.CharField(widget = forms.Textarea(attrs = {
      "class": "form-control",
      "rows": 4,
    }))

    class Meta:
        model = Tweet
        fields = [
            "content",
        ]

class LoginForm(forms.Form):
    username = forms.CharField(widget = forms.TextInput())
    password = forms.CharField(widget = forms.PasswordInput())

class SignUpUsersForm(UserCreationForm):
    first_name = forms.CharField(widget = forms.TextInput())
    last_name = forms.CharField(widget = forms.TextInput())
    email = forms.CharField(widget = forms.EmailInput())
    username = forms.CharField(widget = forms.TextInput())
    password1 = forms.CharField(widget = forms.PasswordInput())
    password2 = forms.CharField(widget = forms.PasswordInput())

    class Meta:
      model = User
      fields = [
          'first_name',
          'last_name',
          'email',
          'username',
          'password1',
          'password2'
      ]
