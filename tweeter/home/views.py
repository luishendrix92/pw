from django.shortcuts import render, redirect
from django.views import generic
from django.urls import reverse_lazy
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User

from .forms import FormTweet, SignUpUsersForm, LoginForm
from .models import Tweet

# ============================
# Class Based Views
# ============================

class List(generic.ListView):
    template_name = "home/list_view.html"
    queryset = Tweet.objects.all()

    def get_context_data(self, *args, **kwargs):
        context = super(List, self).get_context_data(*args, **kwargs)
        context["form"] = FormTweet()
        context["action_url"] = reverse_lazy("create2")

        return context

class Detail(generic.DetailView):
    template_name = 'home/detail.html'
    model = Tweet

class Create(generic.CreateView):
    template_name = 'home/create.html'
    model = Tweet
    form_class = FormTweet
    success_url = reverse_lazy('list_view')

    def form_valid(self, form):
        form.instance.user = self.request.user
        
        return super(Create, self).form_valid(form)

class Update(generic.UpdateView):
    template_name = "home/update.html"
    model = Tweet
    form_class = FormTweet
    success_url = reverse_lazy('list_view')

class Delete(generic.DeleteView):
    template_name = "home/delete.html"
    model = Tweet
    success_url = reverse_lazy('list_view')

class Signup(generic.CreateView):
    template_name = 'home/signup.html'
    model = User
    form_class = SignUpUsersForm
    success_url = reverse_lazy('index_view')

# ============================
# Function Based Views
# ============================

def index(request):
    message = "Not logged in..."
    form = LoginForm(request.POST or None)

    if request.method == "POST":
        if form.is_valid():
            data = form.cleaned_data # requesst.POST["username"]
            user = authenticate(username = data['username'], password = data['password'])

            if user is not None:
                if user.is_active:
                    login(request, user)
                    message = "Logged in"
                else:
                    message = "User not active..."
            else:
                message = "Username or password incorrect..."

    return render(request, 'home/index.html', {
        "form": form,
        "message": message,
    })

# C R U D
def list(request):
    return render(request, 'home/list.html', {
        "object_list": Tweet.objects.all(),
        "form": FormTweet(),
        "action_url": reverse_lazy('create')
    })

def create(request):
    form = FormTweet(request.POST or None)

    if form.is_valid():
        instance = form.save(commit = False)
        instance.user = request.user
        instance.save()

        return redirect('list')
    return render(request, 'home/create.html', { "form": form })

def detail(request, id):
    return render(request, 'home/detail.html', {
        "object": Tweet.objects.get(id = id)
    })

def update(request, id):
    tweet = Tweet.objects.get(id = id)

    if request.method == "GET":
        form = FormTweet(instance = tweet)
    else:
        form = FormTweet(request.POST, instance = tweet)

        if form.is_valid():
            form.save()
            
            return redirect('list')
    return render(request, 'home/update.html', { "form": form })

def delete(request, id):
    tweet = Tweet.objects.get(id = id)

    if request.method == "POST":
        tweet.delete()
        
        return redirect('list')
    return render(request, 'home/delete.html', { "object": tweet })
