"""tweeter URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib.auth.views import LogoutView
from django.contrib import admin
from django.urls import path, include
from django.conf import settings

from home import views

app_name = "home_app"

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name = "index_view"),
    path('list/', views.list, name = "list"),
    path('detail/<int:id>', views.detail, name = "detail"),
    path('create/', views.create, name = "create"),
    path('update/<int:id>', views.update, name = "update"),
    path('delete/<int:id>', views.delete, name = "delete"),
    path('list_view/', views.List.as_view(), name = "list_view"),
    path('detail2/<int:pk>', views.Detail.as_view(), name = "Detail"),
    path('create2/', views.Create.as_view(), name = "create2"),
    path('delete2/<int:pk>', views.Delete.as_view(), name = "delete2"),
    path('update2/<int:pk>', views.Update.as_view(), name = "update_view"),
    path('signup/', views.Signup.as_view(), name = "signup"),
    path('logout/', LogoutView.as_view(), {"next_page": settings.LOGOUT_REDIRECT_URL}, name = "logout"),
    path('api/', include('home.api.urls'))
]
