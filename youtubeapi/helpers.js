function request(url, params = {}, options = {}, jsonResponse = true) {
  const paramString = Object.keys(params)
    .map(k => `${k}=${params[k]}`)
    .join('&')
  const promise = fetch(url + (paramString ? '?' + paramString : ''), options)
  
  return jsonResponse
    ? promise.then((response) => response.json())
    : promise
}

export { request }
