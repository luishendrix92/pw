import React from 'react'

function PlaylistResult({ id, snippet: { title, description, channelTitle, thumbnails, channelId } })  {
  return (
    <div className="ui card">
      <div className="image"><img src={thumbnails.medium.url} /></div>
      <div className="content">
        <a href={`https://youtube.com/playlist?list=${id}`} className="header">{title}</a>
        <div className="meta"><a href={`https://youtube.com/channel/${channelId}`}>{channelTitle}</a></div>
        <div className="description">{description.slice(0, 220)}</div>
      </div>
    </div>
  )
}

export default PlaylistResult
