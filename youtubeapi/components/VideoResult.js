import React from 'react'
import * as yt from '../api'

function VideoResult({ id, snippet: { thumbnails, title, description, channelTitle, channelId }, stats })  {
  async function commentVideo(e) {
    e.persist()
    
    if (e.which === 13) {
      try {
        await yt.insertComment({
          videoId: id, channelId,
          text: e.target.value
        })

        e.target.value = ''
        alert('Comment inserted successfully!')
      } catch(err) {
        alert(err.message)
      }
    }
  }

  async function like(e) {
    try {
      await yt.rateVideo(id, 'like')

      alert('You now like this video!')
    } catch (err) {
      alert(err.message)
    }
  }

  // const tagElements = tags.slice(0,4).map((tag) => <a key={tag} className="ui tag tiny label">{tag}</a>)

  return (
    <div className="ui card">
      <div className="image"><img src={thumbnails.medium.url} /></div>
      <div className="content">
        <a href={`https://youtube.com/watch?v=${id}`} className="header">{title}</a>
        <div className="meta"><a href={`https://youtube.com/channel/${channelId}`}>{channelTitle}</a></div>
        <div className="description">{description.slice(0, 220)}</div>
      </div>
      <div className="extra content">
        <span className="right floated">
          <i className="heart outline like icon" onClick={like}></i>
          {`${stats.likeCount} likes`}
        </span>
        <i className="comment icon"></i>
        {stats.commentCount >= 0 ? `${stats.commentCount} comments` : 'Comments disabled'}
      </div>
      <div className="extra content">
        <div className="ui large transparent left icon input">
          <i className="edit outline icon"></i>
          <input type="text" placeholder="Add Comment..." onKeyUp={commentVideo} />
        </div>
      </div>
    </div>
  )
}

export default VideoResult
