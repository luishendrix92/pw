import React from 'react'
import 'babel-polyfill'
import * as yt from '../api'

class SearchForm extends React.Component {
  state = { query: '', type: 'video' }

  setQuery = (e) => {
    this.setState({ ...this.state, query: e.target.value })
  }

  setType = (e) => {
    this.setState({ ...this.state, type: e.target.value })
  }

  search = async (e) => {
    e.preventDefault()

    const type = this.state.type
    const { items: ids } = await yt.search({ type, q: this.state.query })
    const items = await yt[type]({
      id: ids.map(({ id }) => id[`${type}Id`]).join(',')
    })
    
    this.props.setResults(items.items)
  }
  
  render() {
    return (
      <div>
        <form className="ui form large" onSubmit={this.search}>
          <div className="ui action fluid input">
            <select style={{ 'width': '150px', 'marginRight': '10px' }} onChange={this.setType}>
              <option value="video">Videos</option>
              <option value="channel">Channels</option>
              <option value="playlist">Playlists</option>
            </select>

            <input placeholder="Your query..." onChange={this.setQuery} />
              
            <button className="ui button" type="submit">Search</button>
          </div>
        </form>
      </div>
    )
  }
}

export default SearchForm
