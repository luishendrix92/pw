import React from 'react'
import * as yt from '../api'

function ChannelResult({ id, snippet: { thumbnails, description, title }, stats })  {
  async function subscribeToChannel(e) {
    try {
      await yt.subscribe('channel', id)

      alert('Subscription complete!')
    } catch(err) {
      alert(err.message)
    }
  }

  return (
    <div className="ui card">
      <div className="image"><img src={thumbnails.high.url} /></div>
      <div className="content">
        <a href={`https://youtube.com/channel/${id}`} className="header">{title}</a>
        <div className="description">{description.slice(0, 220)}</div>
      </div>
      <div className="extra content">
        <span className="left floated">
          <i className="users icon"></i>
          {stats.subscriberCount} Subs
        </span>
        <span className="right floated">
          <i className="video icon"></i>
          {stats.videoCount} Videos
        </span>
      </div>
      <div className="ui bottom attached button" onClick={subscribeToChannel}>
        <i className="add icon"></i>
        Subscribe
      </div>
    </div>
  )
}

export default ChannelResult
