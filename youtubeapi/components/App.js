import React from 'react'
import 'babel-polyfill'
import VideoResult from './VideoResult'
import ChannelResult from './ChannelResult'
import PlaylistResult from './PlaylistResult'
import SearchForm from './SearchForm'

class App extends React.Component {
  state = { results: [] }

  setResults = (results) => {
    this.setState({ results: results })
  }
  
  render() {
    const results = this.state.results.map(({ kind, id, snippet, statistics }) => {
      if (kind === 'youtube#video') {
        return <VideoResult key={id} id={id} snippet={snippet} stats={statistics} />
      } else if (kind === 'youtube#channel') {
        return <ChannelResult key={id} id={id} snippet={snippet} stats={statistics} />
      } else if (kind === 'youtube#playlist') {
        return <PlaylistResult key={id} id={id} snippet={snippet} />
      }
    })

    return (
      <div>
        <SearchForm setResults={this.setResults} />
        
        <div id="card-list" className="ui three stackable cards">
          {results}
        </div>
      </div>
    )
  }
}

export default App
