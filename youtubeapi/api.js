import { request } from './helpers'

const apiUrl = 'https://www.googleapis.com/youtube/v3/'
const apiKey = 'AIzaSyAv5z70I-cEUS8_IvK5KDtWpkSStU2pdxw'
const token = 'ya29.GlvgBpV3GrT2YwPjS_evm1dSWOanwDHeA-8qktRcnUZoBbYgAzQQ01r3Id8PrtbU1NPsH28MCxpWe462AnttedzjKB9xvolZY0OJHL_HQXZ2-tec1NfNU33W3TYo'

function insertComment({ channelId, videoId, text }) {
  return request(apiUrl + 'commentThreads', { part: 'snippet' }, {
    method: 'POST',
    headers: {
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      snippet: {
        channelId, videoId,
        topLevelComment: {
          snippet: { textOriginal: text }
        }
      }
    })
  })
}

function search({ type, q }) {
  return request(apiUrl + 'search', {
    part: 'id',
    maxResults: 9,
    key: apiKey, type, q
  })
}

function subscribe(resourceType, resourceId) {
  // TODO: Implement
}

function video({ id }) {
  return request(apiUrl + 'videos', {
    part: 'snippet,statistics',
    key: apiKey, id
  })
}

function channel({ id }) {
  return request(apiUrl + 'channels', {
    part: 'snippet,statistics',
    key: apiKey, id
  })
}

function playlist({ id }) {
  return request(apiUrl + 'playlists', {
    part: 'snippet', key: apiKey, id
  })
}

function rateVideo(id, rating) {
  // TODO: Implement
}

export { 
  insertComment, 
  subscribe, 
  search, 
  video, 
  channel,
  playlist,
  rateVideo
}
