# Apuntes de Programación Web 505

## Miércoles 6 de Marzo de 2019

Instalar django rest framework desde https://www.django-rest-framework.org/ en la termina.

[+] `home/views.py`

```python
from django.core import serializers
from django.http import HttpResponse
```

```python
def wsTweets(request):
    data = serializers.serialize('json', Tweet.objects.all())

    return HttpResponse(data, content_type = 'application/json')
```

[+] `tweeter/urls.py`

```python
    path('wsTweets', views.wsTweet, name = 'wsTweet')
```

## Miércoles 13 de Marzo de 2019

[+] **DIR** `home/api`

[+] **FILE** `home/api/__init__.py`

[+] `home/api/serializers.py`

```python
from rest_framework import serializers
from django.contrib.auth import get_user_model

from home.models import Tweet

model_user = get_user_model()

class UserDisplaySerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = [
            "username",
            "first_name",
            "last_name",
        ]

class TweetModelSerializer(serializers.ModelSerializer):
    user = UserDisplaySerializer()

    class Meta:
        model = Tweet
        fields = ["user", "content"]
```

## Miércoles 10 de Abril de 2019

[+] `templates/form_create.html`

```html
<form ... id='{{ form_id }}' ...>
```

[+] `templates/list.html`

```jinja
{% include ... form_id="tweet_form" %}
```

```javascript
$('tweet-form').submit(function(event) {
    event.preventDefault()

    var this_ = $(this)
    var formData = this_.serialize()

    $.ajax({
        url: '/api2/tweets',
        method: 'POST',
        data: formData,
        success: fetchTweets,
        error: console.error
    })
})
```

