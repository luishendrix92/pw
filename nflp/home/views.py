from django.shortcuts import render

from .models import Stadium
from .models import Team
from .models import Player

# Create your views here.
def index(request):
    return render(request, 'home/index.html', {
        'teams': Team.objects.all()
    })

def detail_team(request, id):
    return render(request, 'home/team.html', {
        'team': Team.objects.get(id = id),
        'roster': Player.objects.filter(team = id)
    })

def list_players(request):
    position_filter = request.GET.get('position', '')

    if position_filter == '':
        return render(request, 'home/players.html', {
            'players': Player.objects.all(),
            'positions': Player.objects.values('position').distinct()
        })
    else:
        return render(request, 'home/players.html', {
            'players': Player.objects.filter(position = position_filter),
            'positions': []
        })

def detail_player(request, id):
    return render(request, 'home/player.html', {
        'player': Player.objects.get(id = id)
    })

def list_stadiums(request):
    return render(request, 'home/stadiums.html', {
        'stadiums': Stadium.objects.all()
    })

def detail_stadium(request, id):
    return render(request, 'home/stadium.html', {
        'stadium': Stadium.objects.get(id = id)
    })
