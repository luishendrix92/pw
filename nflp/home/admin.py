from django.contrib import admin

from .models import Stadium
from .models import Team
from .models import Player

# Register your models here.
@admin.register(Stadium)
class AdminStadium(admin.ModelAdmin):
    list_display = [
        'id',
        'name',
        'description',
        'capacity',
        'address',
        'location',
        'photo',
    ]

@admin.register(Team)
class AdminTeam(admin.ModelAdmin):
    list_display = [
        'id',
        'name',
        'description',
        'website',
        'logo',
        'stadium',
        'owner',
        'head_coach',
    ]

@admin.register(Player)
class AdminPlayer(admin.ModelAdmin):
    list_display = [
        'id',
        'full_name',
        'age',
        'birthplace',
        'height',
        'weight',
        'position',
        'team',
        'photo',
    ]
