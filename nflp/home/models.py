from django.db import models
from django.conf import settings

# Create your models here.
class Stadium(models.Model):
    name = models.CharField(max_length = 50)
    description = models.CharField(max_length = 500)
    capacity = models.IntegerField()
    address = models.CharField(max_length = 150)
    location = models.CharField(max_length = 50)
    photo = models.CharField(max_length = 50)

class Team(models.Model):
    name = models.CharField(max_length = 50)
    description = models.CharField(max_length = 500)
    website = models.CharField(max_length = 100)
    logo = models.CharField(max_length = 50)
    stadium = models.ForeignKey(Stadium, on_delete = models.CASCADE)
    owner = models.CharField(max_length = 100)
    head_coach = models.CharField(max_length = 100)

class Player(models.Model):
    full_name = models.CharField(max_length = 100)
    age = models.IntegerField()
    birthplace = models.CharField(max_length = 100)
    height = models.CharField(max_length = 100)
    weight = models.IntegerField()
    position = models.CharField(max_length = 50)
    team = models.ForeignKey(Team, on_delete = models.CASCADE)
    photo = models.CharField(max_length = 50)
