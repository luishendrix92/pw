"""nflp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from home import views

app_name = "home_app"

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name = 'index_view'),
    path('team/<int:id>', views.detail_team, name = 'detail_team'),
    #path('team/create/', views.create_team, name = 'create_team'),
    path('player/list', views.list_players, name = 'list_players'),
    path('player/<int:id>', views.detail_player, name = 'detail_player'),
    path('stadium/list', views.list_stadiums, name = 'list_stadiums'),
    path('stadium/<int:id>', views.detail_stadium, name = 'detail_stadium'),
]
