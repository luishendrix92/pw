from django.contrib import admin
from django.urls import path

from home import views

# app_name = "home_app"

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.landing, name = 'landing_view'),
    path('project/<int:id>/update/', views.update_project, name = 'update_project'),
    path('project/<int:id>', views.detail_project, name = 'detail_project'),
    path('login/', views.login_view, name = 'login_view'),
    path('logout/', views.logout_view, name = 'logout_view'),
]
