from django.contrib import admin

from .models import EmployeeProfile, Project, Activity, Task

@admin.register(EmployeeProfile)
class AdminEmployeeProfile(admin.ModelAdmin):
    list_display = [
        'user',
        'bio',
        'picture',
        'birthdate',
        'phone',
    ]

@admin.register(Project)
class AdminProject(admin.ModelAdmin):
    list_display = [
        'name',
        'repo_url',
        'icon',
        'description',
        'leader',
        'due_date',
    ]

class TaskInline(admin.TabularInline):
    model = Task

@admin.register(Activity)
class AdminActivity(admin.ModelAdmin):
    list_display = [
        'id',
        'project',
        'title',
        'description',
        'deadline',
    ]

    inlines = [
        TaskInline,
    ]

@admin.register(Task)
class AdminTask(admin.ModelAdmin):
    list_display = [
        'id',
        'activity',
        'title',
        'completed',
    ]
