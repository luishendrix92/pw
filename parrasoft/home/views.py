from django.shortcuts import render, redirect
from django.contrib.auth import logout, login, authenticate, get_user_model
from datetime import datetime
from math import floor

from .forms import LoginForm, ProjectForm
from .models import EmployeeProfile, Project, Task

"""--------------------
INDEX LANDING PAGE VIEW
--------------------"""

def landing(request):
    if request.user.is_authenticated:
        return render(request, 'home/dashboard.html', {
            'profiles': EmployeeProfile.objects.all(),
            'projects_n': Project.objects.count(),
            'current_projects': Project.objects.filter(due_date__gte = datetime.now()),
            'completed_n': Project.objects.exclude(activity__task__completed = False).count(),
            'in_progress_n': Project.objects.filter(activity__task__completed = False, due_date__gte = datetime.now()).distinct().count(),
            'overdue_n': Project.objects.filter(activity__task__completed = False, due_date__lt = datetime.now()).distinct().count(),
            'tasks_n': Task.objects.count(),
            'tasks_n_complete': Task.objects.filter(completed = True).count(),
            'tasks_n_pending': Task.objects.filter(completed = False).count(),
        })
    else:
        return render(request, 'home/landing.html', { 'form': LoginForm })

"""------------------
LIST AND DETAIL VIEWS
------------------"""
def detail_project(request, id):
    project = Project.objects.get(id = id)
    percentage = floor(
        Task.objects.filter(activity__project_id = project.id, completed = True).count() /
        Task.objects.filter(activity__project_id = project.id).count() * 100
    )

    return render(request, 'home/detail_project.html', { 'project': project, 'percentage': percentage })

"""----------------------
CREATION AND UPDATE VIEWS
----------------------"""

def update_project(request, id):
    project = Project.objects.get(id = id)

    if request.method == "GET":
        form = ProjectForm(instance = project)
    else:
        form = ProjectForm(request.POST, instance = project)

        if form.is_valid():
            form.save()
            
            return redirect('landing_view')
    return render(request, 'home/update_project.html', { "form": form })

"""----------------------
USER AUTHENTICATION VIEWS
----------------------"""

def login_view(request):
    form = LoginForm(request.POST or None)

    if form.is_valid():
        username = form.cleaned_data.get('username')
        password = form.cleaned_data.get('password')
        user = authenticate(username = username, password = password)

        login(request, user)
        return redirect('landing_view')
    else:
        return render(request, 'home/landing.html', { 'form': form })

def logout_view(request):
    logout(request)

    return redirect('landing_view')
