from django.db import models
from django.conf import settings

class EmployeeProfile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete = models.CASCADE)
    bio = models.CharField(max_length = 500, blank = True)
    picture = models.CharField(max_length = 50, default = 'unknown.jpg')
    birthdate = models.DateField(auto_now = False, auto_now_add = False, blank = True, null = True)
    phone = models.CharField(max_length = 50, blank = True)

class Project(models.Model):
    name = models.CharField(max_length = 100)
    repo_url = models.CharField(max_length = 100)
    icon = models.CharField(max_length = 50, default = 'blank.png')
    description = models.CharField(max_length = 500)
    leader = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete = models.CASCADE, related_name = 'lead_projects')
    due_date = models.DateTimeField(auto_now = False, auto_now_add = False)
    team = models.ManyToManyField(settings.AUTH_USER_MODEL)
    created = models.DateTimeField(auto_now_add = True)

class Activity(models.Model):
    project = models.ForeignKey(Project, on_delete = models.CASCADE)
    title = models.CharField(max_length = 200)
    description = models.CharField(max_length = 500)
    deadline = models.DateTimeField(auto_now = False, auto_now_add = False)
    created = models.DateTimeField(auto_now_add = True)

class Task(models.Model):
    activity = models.ForeignKey(Activity, on_delete = models.CASCADE)
    title = models.CharField(max_length = 200)
    completed = models.BooleanField(default = False)
    created = models.DateTimeField(auto_now_add = True)
