from django import forms
from django.contrib.auth import authenticate

from .models import Project

class LoginForm(forms.Form):
    username = forms.CharField(required = True)
    password = forms.CharField(required = True, widget = forms.PasswordInput)

    def clean(self, *args, **kwargs):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        user = authenticate(username = username, password = password)

        if not user:
            raise forms.ValidationError('Username or password incorrect')
        elif not user.is_active:
            raise forms.ValidationError('This employee is not active')

        return super(LoginForm, self).clean(*args, **kwargs)

class ProjectForm(forms.ModelForm):
    repo_url = forms.URLField()

    class Meta:
        model = Project
        fields = '__all__'
        widgets = {
            'repo_url': forms.widgets.URLInput(),
            'description': forms.Textarea(attrs = { 'rows': 2 }),
            'leader': forms.widgets.Select(attrs = { 'class': 'ui dropdown search' }),
            'team': forms.widgets.SelectMultiple(attrs = { 'class': 'ui search dropdown' }),
        }
