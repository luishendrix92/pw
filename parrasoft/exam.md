# Instrucciones del Examen

En la empresa ParraSoft se requiere de una aplicación web que ayude al gerente a organizar las actividades realizadas en la empresa. El gerente requiere saber qué actividades se están realizando al momento de la consulta, qué personal está asignado a esa actividad, quién es el lider de la actividad, y el estatus de la actividad; así también quiere saber cuándo fue asignada y cuándo debe terminar dicha actividad. El gerente también requiere tener reportes que le ayuden a visualizar el desempeño de su personal.

## Importante

Realizar las vistas, modelos, urls, formas, y demás cosas necesarias para complir con las necesidades d elos reportes. Los reportes deben obtener su información mediante un queryset al modelo. Generar las sesiones y/o autentificaciones de usuarios necesarias.
